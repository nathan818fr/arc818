# IntelliJ

1. Set the application theme:
    - Install the plugin "Arc Theme Dark" (by Pavel Zlámal)
    - Apply it

2. Set the editor theme:
    - Editor > Color Scheme > Import intellij-darcula_arc818.icls
    - Apply it
