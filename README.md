# Arc818

This repository contains the instructions and configurations of my personal
Linux theme.

My window manager is **Cinnamon**.

![preview.png](preview.png)

## Installation

### Theme (Gtk, Cinnamon, ...)

Install or update it with:

```bash
curl -fsSL 'https://gitlab.com/api/v4/projects/27512656/releases' |
  jq -Mre 'first(.[].assets.links[]|select(.name=="Arc818-theme.tar.gz")|.url)' |
  xargs curl -fSL |
  sudo tar -xzv --strip-components=1 --no-same-owner -C /usr/share/themes
```

(note: theme will be installed in `/usr/share/themes` instead
of `/usr/local/share/themes` to be compatible with qt5ct)

Then apply it:

- Window borders: Arc818-Dark
- Controls: Arc818-Dark
- Desktop: Arc818-Dark

### Icons

Install or update it with:

```bash
curl -fsSL 'https://gitlab.com/api/v4/projects/28109143/releases' |
  jq -Mre 'first(.[].assets.links[]|select(.name=="Arc818-icons.tar.gz")|.url)' |
  xargs curl -fSL |
  sudo tar -xzv --strip-components=1 --no-same-owner -C /usr/share/icons
```

(note: icons will be installed in `/usr/share/icons` instead
of `/usr/local/share/icons` to be compatible with qt5ct)

Then apply it:

- Icons: Arc818

### Qt5

You can use the Gtk theme for Qt applications using Qt5 CT.

Install it with:

```bash
sudo apt install qt5ct qt5-style-plugins
sudo tee /etc/environment.d/90qt-ct.conf <<EOF >/dev/null
QT_QPA_PLATFORMTHEME=qt5ct
QT_PLATFORMTHEME=qt5ct
QT_PLATFORM_PLUGIN=qt5ct
EOF
```

(note: you should restart your computer)

Then configure it:\
Qt5 CT > Style: gtk2

## Cinnamon Configuration

See [cinnamon/README](cinnamon/README.md).

## Apps Configuration

You can also configure specific applications for better integration:

- [gedit](apps/gedit)
- [IntelliJ](apps/intellij)
- [Tilix](apps/tilix)

# Notes

You can also consult the other repositories used:

- [arc818-theme](https://gitlab.com/nathan818/arc818-theme/-/releases)
- [arc818-icons](https://gitlab.com/nathan818/arc818-icons/-/releases)

You can uninstall the theme and icons with the following command:

```bash
sudo find \
  /usr/share/themes /usr/local/share/themes ~/.local/share/themes ~/.themes \
  /usr/share/icons /usr/local/share/icons ~/.local/share/icons ~/.icons \
  -mindepth 1 -maxdepth 1 -type d \( -name 'Arc818' -o -name 'Arc818-*' \) \
  -exec echo 'Delete {} ...' \; -exec rm -rf -- {} \;
```
