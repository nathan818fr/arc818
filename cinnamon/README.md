# Cinnamon

## Settings

### Themes

![themes_1.png](.assets/themes_1.png)

### Font Selection

![font_1.png](.assets/font_1.png)

### Effects

![effects_1.png](.assets/effects_1.png)
![effects_2.png](.assets/effects_2.png)

### Windows

![windows_1.png](.assets/windows_1.png)
![windows_2.png](.assets/windows_2.png)

### Workspaces

![img.png](.assets/workspaces_1.png)

### Extension: User Shadows

![user_shadows_1.png](.assets/user_shadows_1.png)

## Panel - Primary Display

Bottom panel applets (in order):

- Left:
    - Cinnamenu
    - CobiWindowList
- Right:
    - Collapsible Systray
    - Force Quit
    - Notifications
    - Favorites
    - Printers
    - Removable drives
    - Network Manager
    - Sound
    - Power Manager
    - Keyboard
    - Calendar
    - Workspace switcher

### Cinnamenu

![cinnamenu_1.png](.assets/cinnamenu_1.png)
![cinnamenu_2.png](.assets/cinnamenu_2.png)
![cinnamenu_3.png](.assets/cinnamenu_3.png)
![cinnamenu_4.png](.assets/cinnamenu_4.png)

Panel icon file: [menu.svg](menu.svg)

### CobiWindowList

![cobiwindowlist_1.png](.assets/cobiwindowlist_1.png)

### Collapsible Systray

![collapsiblesystray_1.png](.assets/collapsiblesystray_1.png)
![collapsiblesystray_2.png](.assets/collapsiblesystray_2.png)

### Calendar

![calendar_1.png](.assets/calendar_1.png)

Custom date format: `%Y-%m-%d %H:%M:%S`

### Workspace switcher

![workspaceswitcher_1.png](.assets/workspaceswitcher_1.png)

## Panel - Secondary Displays

Like the Primary Display Panel, but only the Left section.
