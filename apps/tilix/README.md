# Tilix

1. Install:
   ```bash
   sudo apt install tilix
   ```

2. Restore configuration:
   ```bash
   dconf load /com/gexperts/Tilix/ < tilix.dconf
   ```

3. Set as default terminal:\
   System Settings > Preferred Applications > and select Terminal
