# gedit

1. Install:
   ```bash
   sudo apt install gedit
   ```

2. Install theme:
   ```bash
   mkdir -p ~/.local/share/gedit/styles
   cp 818-style.xml ~/.local/share/gedit/styles/818.xml
   ```

3. Set the theme:\
   gedit > Preferences > Fint & Colors > set Color Scheme to "818"
